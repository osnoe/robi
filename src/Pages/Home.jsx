import React, { Component } from "react";
import Typing from "react-typing-animation";
import "../Styles/Home.css";
import RobiPet from "../Assets/robi-pet.png";
import Book from "../Assets/book.svg";
import Axios from "axios";

export default class Home extends Component {
  //   const [robiText, setRobiText] = useState("Tell me what you want to read");
  constructor(props) {
    super(props);
    this.state = {
      robiText: "Tell me what you want to read",
      userInput: "",
      loading: false,
        books: [],
    //   books: [
    //     {
    //       count: 2,
    //       genre: "Horror",
    //       title: "It",
    //     },
    //     {
    //       count: 1,
    //       genre: "Horror",
    //       title: "The Shining",
    //     },
    //     {
    //       count: 1,
    //       genre: "Religion",
    //       title: "Zealot: The Life and Times of Jesus of Nazareth",
    //     },
    //     {
    //       count: 1,
    //       genre: "Gay and Lesbian",
    //       title: "Carry On",
    //     },
    //   ],
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  changeUserInput(text) {
    this.setState({
      ...this.state,
      userInput: text,
    });
  }

  animate(text) {
    console.log(text);
    this.setState({
      robiText: null,
    });
    setTimeout(() => {
      this.setState({
        robiText: text,
      });
    }, 300);
  }

  reset() {
    this.setState({
    //   robiText: "Tell me what you want to read",
      userInput: "",
      loading: false,
      books: [],
    });

    this.animate("Ok lets try again!")
  }

  onSubmit = () => {
    console.log(this.state.userInput);
    this.getBooks(this.state.userInput);
  };

  async getBooks(text) {
    this.animate("Loading");
    this.setState({
      ...this.state,
      loading: true,
    });
    let response = await Axios.post(
      "http://localhost:5000/api/books/recomendations",
      {
        user_input: text,
      }
    );

    if (response.data) {
      this.setState({
        ...this.state,
        books: response.data,
      });
      console.log(this.state.books, response.data);
      this.setState({
        ...this.state,
        loading: false,
      });
      this.animate("Hope you enjoy them!");
    } else {
      alert("Something wrong happend!");
      this.setState({
        ...this.state,
        loading: false,
      });
      this.animate("Oops! my fault, try again!");
    }
  }

  render() {
    return (
      <div className="main">
        <div className="robi">
          <div>ROBI</div>
          <button onClick={() => this.reset()} className="reset">Reset</button>
        </div>
        <div className="container">
          <img src={RobiPet} className="robi-pet" alt="Robi pet"></img>
          <div className="user-input" onSubmit={this.onSubmit}>
            <div className="robi-text">
              {this.state.robiText ? (
                <Typing>
                  <span>{this.state.robiText}</span>
                </Typing>
              ) : null}
            </div>
            <div className="flex-container">
              {this.state.books.map((book) => {
                return <Card key={book.title} book={book}></Card>;
              })}
            </div>
            <div className="flex-container">
              <input
                disabled={this.state.loading}
                value={this.user_input}
                onChange={(event) => this.changeUserInput(event.target.value)}
                placeholder="I like it when characters have a hate-love relationship wit|"
              ></input>
            </div>

            <button
              disabled={this.state.loading}
              className="search-book"
              onClick={this.onSubmit}
            >
              {" "}
              Search book{" "}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export function Card(props) {
  return (
    <div className="book-card">
      <div className="flex-container">
        <img src={Book} alt="Book"></img>
      </div>
      <div className="flex-container">
        <b>{props.book.title}</b> | {props.book.genre}
      </div>
    </div>
  );
}
